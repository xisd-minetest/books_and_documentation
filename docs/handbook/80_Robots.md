<!-- --------------------------------------------
-- title : Robots
------------------------------------------------- -->

# Robots

![Image_Robots](imgs/robots.png)

Les robots sont des éléments programmable. Ils sont constitués de deux parties principales : 

* Le 'spawner' (ou 'cpu-box'), est la partie que vous pouvez crafter. Posez le et écrivez du code depuis son interface.
* Le robot (ou 'worker') apparaît au dessus du spawner lorsque que vous activez le bouton 'start'. Ce sera alors ce 'worker' qui executera le code.

Vous pouvez aussi crafter un terminal qui contient quelques exemples de scripts pour le robot.

Reportez vous aux [manuels techniques](../#cookbooks) pour plus de détails.
