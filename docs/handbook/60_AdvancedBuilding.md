<!-- --------------------------------------------
-- title : Construction avancée
------------------------------------------------- -->

# Construction avancée

### Le tournevis

![Image_Screwdriver](imgs/screwdriver.png)

Le tournevis (screwdriver) est un outil qui vous permet de tourner des blocs.
( clic gauche axe vertical, clic-droit axe horizontal )

Cela vous sera particulièrement utile pour tourner des blocs orientés tels que les escaliers ou les dalles.

### L'atelier

![Image_Workbench](imgs/workbench.png)

L'atelier (workbench) permet de découper certain blocs en plus petits morceaux afin de multiplier encore vos possibilités de constructions.


### Préparer ses constructions

Pour trouver de l'inspiration pour vos constructions, vous aurez peut être besoin de vous appuyer sur des exemples en images. Il sont faciles à trouver sur le web, et même s'il n'utilisent pas toujours les mêmes blocs ou les même textures de blocs que ceux qui vous sont disponibles, il offrent souvent des pistes intéressantes.

![Image_Webcastle](imgs/castle.png)

Et à mesure que vous entreprendrez des constructions complexes, il vous sera peut être utile de consulter des gabarits (ex. pour obtenir une forme cercle plus facilement), peut être même de faire des plans, ...

![Image_Circles](imgs/circles.png)


Reportez vous aux [manuels techniques](../#cookbooks) pour plus de détails.

