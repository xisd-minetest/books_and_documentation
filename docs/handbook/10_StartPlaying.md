<!-- --------------------------------------------
-- title : Commencer à jouer
------------------------------------------------- -->

# Ok, et ensuite ?

Un monde gigantesque s'offre a vous, explorez-le, trouvez un coin sympa, construisez, creusez, craftez ...

![Image_landscape](imgs/landscape1.png)

## L'inventaire et la grille de craft

En ouvrant la fenêtre d'inventaire ( I ), vous trouverez une grille de craft au dessus de celui-ci.

![Image_Craft_Grid](imgs/craftgrid1.png)

Certaines combinaisons d'objets placés dans cette grille vous permettent d'obtenir de nouvelles choses. (Par exemple placez un morceau de charbon au dessus d'un baton pour obtenir une torche.) On appelle ce processus "crafter"

Consultez le "Craftguide" pour connaitre les possibilité de craft


## Les matériaux de base

Lors de vos explorations, vous trouverez bon nombre de matériaux directement utilisable pour vos constructions : du bois, du sable, de la terre, ...

![Image_TreeDiversity](imgs/tree_diversity.png)

Vous pourrez aussi fabriquer des outils afin de faciliter la récolte de ces matériaux.

![Image_CraftPick](imgs/craft_pick.png)

Certains matériaux, ne peuvent d'ailleurs être récoltés qu'avec des outils spéciaux.
Vous aurez par exemple besoin d'une pioche afin de pouvoir creuser la pierre.

![Image_Digging](imgs/digging.png)

Ce pioche vous sera aussi utile pour collecter du minerai que vous pourrez cuire et utiliser pour fabriquer certains matériaux ou des outils plus performants.

![Image_ores](imgs/ores1.png)

Vous devrez chercher de plus en plus profond et utiliser des outils plus robustes pour collecter les minerais les plus rares. 


## Barre de vie et ossements

En bas de votre écran, vous apprecevrez une barre de vie.
Qquelques éléments du jeu peuvent vous blesser (feu, noyade, chute, monstres) mais les joueurs ne peuvent pas directement s'attaquer entre-eux.

![Image_Lifebar](imgs/lifebar.png)

Vous pouvez récupérer de la vie grâce à la nourriture.
Lorsqu'un joueur meurt, il réapparait au dernier checkpoint (son lit ou son point de départ) et peut retourner chercher son inventaire en rammassant le bloc ossements à l'endroit de sa mort.

![Image_Bones](imgs/bones1.png)

Utilisez la commande `/showbones` pour retrouver facilement vos ossements. (voir §commandes plus bas)

## Les lits

![Image_Bed](imgs/bed1.png)

Les lit on deux fonctions : 

1. Si au moins la moitiée des joueurs sont au lit, la nuit est passée ( le jour se lève ). 

2. Si vous mourrez, vous réapparaitrez automatiquement à l'emplacement du dernier lit dans lequel vous avez dormit. 

## Les portails :

Vous pouvez crafter des portails afin de retourner facilement à un endroit précis.

![Image_TreePortal](imgs/portals_tree1.png)

Il en existe différents types, par exemple, posez un "portail d'Arbre" (Tree Portal) sur de la terre et récoltez ses racines, vous pourrez ensuite revenir à ce portail en plantant ces racines sur de la terre.

Cf. [Portails](#50_Portals.md) pour plus de détails sur les portails

## Les touches du clavier :

![Image_KeyConfig](imgs/configkeys.png)

Vous pouvez consulter/changer les touches du clavier depuis le menu (echap).

* Maintenez la touche "marcher lentement" (MAJ par default) pour ne pas tomber lorsque vous approchez du bord d'une falaise

* Certaines touches ne fonctionnent que dans des conditions particulières, Par exemple :

	- Certaines potions vous permettent d'activer le mode vol, le reste du temps la touche K n'a aucun effet visible.

	- Vous ne pouvez afficher la mini-carte (F9) qui si vous avez un "mapping kit" dans votre inventaire.

	- Vous devez aussi avoir des jummelles pour Zoomer (W). 

## Les commandes :

Vous pouvez ouvrir le menu du 'chat' avec la touche F10.

Vous pouve aussi taper des commandes commençant par / dans ce menu.

![Image_ChatCommandExemple](imgs/chatcommands.png)

Utiliser la commande `/help` pour lister les commandes disponible, `/help <nom de la commande>` pour obtenir des détails sur une commande spécifique ou `/help all` pour une liste détaillée de toutes les commandes.

> Attention, certaines commandes peuvent avoir des conséquences ( ex: `/killme` vous tue instantanément ou `/clearinv` vide tout votre inventaire).

Utilisez la touche T pour saisir rapidement un message dans le chat.

Utilisez la touche / pour saisir rapidement une commande.
