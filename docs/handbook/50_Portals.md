<!-- --------------------------------------------
-- title : Portails Élementaux
------------------------------------------------- -->

# Portails Élementaux

![Image_Portals](imgs/portals.png)

Les portails élémentaux sont des portails de téléportation, les portails de même type sont reliés entre eux. Il peuvent être activés ponctuellent s'il sont alimentés avec l'élément correspondant ou bien de façn permante s'il sont entourés de leur élément.

Les portails métallique (acier et bronze) sont utilisable par les autres joueurs, les autres sont spécifique à celui ou celle qui les a placés.

![Image_NetherPortal](imgs/NetherPortal.png)

Il existe un autre type de portail, en forme de fenêtre constitutée de 4 blocs d'obsidienne de large sur 3 blocs de hauteur. Une fois activé avec un cristal de mese, celui-ci vous conduira vers le [Nether](#55_Places.md).
