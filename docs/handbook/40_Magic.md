<!-- --------------------------------------------
-- title : Magie et potions
------------------------------------------------- -->

# Magie et potions

Vous n'avez naturellement pas la capacité de voler, ou de courir à grande vitesse, etc.

![Image_BeaconPotions](imgs/beacon_potions.png)

Pour obtenir de tels effets, vous pourrez avoir recours à la magie sous différentes formes.

* Les balises (beacons) sont des blocs infusés d'énergie magique qui affecte une zone alentour, ils diffusent aussi un puissant rayon lumineux au dessus d'eux.

* Les potions peuvent être craftées grâce à un chaudron (A faire !), vous devez les avoir en mains pour les utiliser

* Les haricots magiques ne peuvent peuvent être craftés, mais se trouvent parfois dans les hautes herbes, certains poussents et forment une gigantesque tige grimpable, plantez-les pour obtenir leurs effets (A faire !)
