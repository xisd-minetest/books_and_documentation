<!-- --------------------------------------------
-- title : Lieux spéciaux
------------------------------------------------- -->

# Quelques lieux particuliers 

### Le Nether

![Image_Nether](imgs/nether1.png)

Le Nether est un environnement hostile, peuplé de monstres (A faire !) situé à plus de -25000 blocs de profondeur, vous y rencontrerez des matériaux impossibles a trouver ailleurs ainsi que la possibilité d'obtenir certains obets spéciaux (A faire !).

Il est possible de s'y rendre grace aux portails d'obsidienne. (cf. [Portails](#55_Portals.md)) 

### Autres lieux  (A faire !)


