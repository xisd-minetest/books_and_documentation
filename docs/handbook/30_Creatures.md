<!-- --------------------------------------------
-- title : Animauxs et créatures
------------------------------------------------- -->

# Animaux et Créatures

La plupart des créatures que vous rencontrerez sont pacifiques, seuls quelques monstres vous seront hostiles, ceux-ci se rencontrent généralement en profondeur, dans les caves mal éclairées ou dans des endroits spéciaux (pyramides, donjons, nether,...).

Il est généralement possible d'obrtenir des informations sur une créature grace à la loupe.

## Animaux volants

![Image_Mob](imgs/mobs_bird_inv.png)
![Image_Mob](imgs/mobs_gull_inv.png)

## Animaux terrestres

![Image_Mob](imgs/mobs_sheep_inv.png)
![Image_Mob](imgs/mobs_bunny_inv.png)
![Image_Mob](imgs/mobs_chicken_inv.png)
![Image_Mob](imgs/mobs_kitten_inv.png)
![Image_Mob](imgs/mobs_penguin_inv.png)
![Image_Mob](imgs/mobs_turtle_inv.png)
![Image_Mob](imgs/mobs_green_slime_inv.png)

## Animaux Marins

![Image_Mob](imgs/mobs_clownfish_inv.png)
![Image_Mob](imgs/mobs_jellyfish_inv.png)
![Image_Mob](imgs/mobs_seaturtle_inv.png)


## Monstres

![Image_Mob](imgs/mobs_lava_flan_inv.png)
![Image_Mob](imgs/mobs_mummy_inv.png)
![Image_Mob](imgs/mobs_oerkki_inv.png)
![Image_Mob](imgs/mobs_zombie_inv.png)
