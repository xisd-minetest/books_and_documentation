<!-- --------------------------------------------
-- title : Introduction
------------------------------------------------- -->

# Debuter dans minetest


Lors de votre première connexion au serveur, vous verrez apparaïtre un menu vous proposant de choisir un kit de départ.

![Image_StartingKits](imgs/starting_kits.png)

Ce choix détermine les outils et blocs avec lesquels vous démarrez le jeu, cela vouz permet d'accèder plus rapidement à un aspect donné du jeu mais ne limite pas vos possibilités.

> ( Par exemple, avec le kit "mineu·r·se", vous obtiendrez directement des rails et une pioche en acier, mais si vous avez choisit un autre kit, rien ne vous empêche de réunir par ensuite les matériaux nécessaire à la fabrication des rails et d'une pioche en acier. )

Quelque soit votre kit de départ, vous obtiendrez :

* Le "Craftguide" 

	![Image_Craftguide](imgs/craftguide_book.png)
	
	Il contient toutes les recettes de 'craft'

* Un "Abris pré-fabriqué"

	![Image_Shelter](imgs/shelter.png)
	
	Choisissez un endroit bien dégagé et placez-le.
	
	Fouillez ensuite à l'interieur pour trouver, entre autres choses, la **loupe** qui vous permettra d'obtenir des informations sur les blocs que vous rencontrerez par la suite.
