<!-- --------------------------------------------
-- title : Quêtes et trame narrative
------------------------------------------------- -->

# Quêtes et trame narrative

## Trophés

![Image_Awards](imgs/awards1.png)

Dans l'onglet Trophés de votre inventaire, vous trouverez une liste de réussites et aux conditions a remplir pour les débloquer.

Quelques rares trophées sont récompensés par une capacitée ou un objet spécial. (A faire !)

## Apparence

![Image_Skins](imgs/skins1.png)

Lors de votre première connexion, une apparence vous est attributée en fonction du nom de votre personnage.

Vous pouvez voir votre personnage avec la touche F7, et changer d'apparence en craftant une garde-robe ou depuis l'onglet Skins de votre inventaire.


## Elements narratifs

![Image_Bookshelves](imgs/story1.png)

Histoire (A faire !)
Le monde dans lequels vous vous trouvez abritait jadis une grande et prospère civilisation. Les raisons de son déclin sont inconnues mais il semblerai qu'un évenement terrible ait laissé les survivant de ce peuple éclairé transformés en montres écervelés errants sans but dans l'obscurité blah blah blah.

Lors de vos exploration vous rencontrerez certains vestige de cette époque lointaine : anciennes pyramides, ruines, mines désafectées, etc.. et serez peut être à même de faire la lumière sur les évenements du passé.

## Les PNJs (à faire)

Lors de vos explorations vous recontrerez divers PNJs, certains aurons des choses a vous raconnter, et se souviendrons parfois de vous, d'autres vous proposerons un échange, ou vous demanderons de l'aides, etc... 

## Objets spéciaux  (A faire !)

Certains objets spéciaux ne peuvent pas être craftés, vous devrez les trouver ou les obtenir en récompense de certaines actions.

>(Objets prévus : 
> machine à collecter les blocs sans les casser, 
> moyen de convertir les zombies en npc (et inversement),
> moyens d'apprivoiser certains monstres,
> privilège de vol, ...
>)

## Puzzles, Défis et Quètes (A faire !)

Certains puzzles, defis ou quêtes vous donnerons accès à des bonus ou à certaines zones ou à des objets ou capacités spéciales. 
