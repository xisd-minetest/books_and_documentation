<!-- --------------------------------------------
-- title : Agriculture et nourriture
------------------------------------------------- -->

# Plantes, agriculture et nourriture

Il existe différents types de plantes cultivables. Certaines poussent à l'état sauvage et peuvent être récoltées. Pour les autres, leurs graines peuvent généralement se trouver en rammassant de l'herbe ou herbe de jungle, les pommes de terre peuvent être trouvée dans la terre.

![Image_ToolHoe](imgs/farming_tool_stonehoe.png)

La plupart des légumes et graines ne pousseront que si elles sont plantée à proximité d'un point d'eau sur un sol travaillé avec une **Houe**

![Image_CropSeeds](imgs/crop_seeds.png)

Ces conditions ne sont pas nécessaires pour les arbres et cacti mais les papyrus doivent être directement voisins d'une case d'eau

La plupart des plantes fonctionnent de la même façon : plantez une jeune pousse ou des graines dans les bonne condition, et laissez pousser.

Certaines plantes telles que les tomates ou les potirons peuvent être récoltées plusieurs fois avant de changer le pieds

D'autres telles que les haricots verts ne peuvent être plantée qu'au pied d'un tuteur (beanpole).

![Image_CropSeeds](imgs/crop_grown.png)
