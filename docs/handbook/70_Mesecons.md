<!-- --------------------------------------------
-- title : Mesecons & tubes
------------------------------------------------- -->

# Mesecons & tubes

![Image_Mesecons](imgs/mesecons.png)

Grace aux mesecons vous pouvez construire toutes sortes de circuits, automatiser la récolte de certains blocs, créer des passages secrets, déplacer des objets d'un endroit à l'autre dans des tubes, ...

![Image_Pipeworks](imgs/pipeworks.png)


Reportez vous aux [manuels techniques](../#cookbooks) pour plus de détails.


