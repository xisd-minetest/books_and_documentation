<!-- --------------------------------------------
-- title : Robot API
-- author : 
-- description : Robot functions and syntax usable inside robots
-- source : In-Game Help from basic_robot mod
--  converted to markdown syntax but still importable into a formspec
-- (...still waiting for in-game markdown parser)				
------------------------------------------------- -->

# Robot API
This document present most of functions and syntax usable inside robots,for details about behaviors, limitations and usages, see 'About Robots'

### BASIC LUA SYNTAX

most of basic lua syntax allowed
>	if x==1 then A else B end
>	for i = 1, 5 do something end
>	while i<6 do A; i=i+1; end

arrays:

>	myTable1 = {1,2,3},  myTable2 = {[\"entry1\"]=5, [\"entry2\"]=1}

access table entries with myTable1[1] or myTable2.entry1 or myTable2[\"entry1\"]

( See lua syntax for more detailled informations )

## ROBOT COMMANDS

### MOVEMENT,DIGGING, PLACING, INVENTORY TAKE/INSERT

`move.direction()` where direction is forward, backward, left,right, up, down), forward_down direction only works with dig, place and read_node
`turn.left()`, `turn.right()`, `turn.angle(45)`
`dig.direction()`
`place.direction(\"default:dirt\", optional orientation param)`
`read_node.direction()` tells you names of nodes
`insert.direction(item, inventory)` inserts item from robot inventory to target inventory
`check_inventory.direction(itemname, inventory, index)` looks at node and returns false/true, direction can be self,
 if index>0 it returns itemname. if itemname == \"\" it checks if inventory empty
`activate.direction(mode)` activates target block
`pickup(r)` picks up all items around robot in radius r<8 and returns list or nil
`craft(item,mode)` crafts item if required materials are present in inventory. mode = 1 returns recipe
`take.direction(item, inventory)` takes item from target inventory into robot inventory
`read_text.direction(stringname,mode)` reads text of signs, chests and other blocks, optional stringname for other meta, mode 1 read number
`write_text.direction(text,mode)` writes text to target block as infotext

### BOOKS/CODE

`title,text=book.read(i)` returns title,contents of book at i-th position in library
`book.write(i,title,text)` writes book at i-th position at spawner library
`code.run(text)` compiles and runs the code in sandbox
`code.set(text)` replaces current bytecode of robot
`find_nodes(\"default:dirt\",3)` returns distance to node in radius 3 around robot, or false if none

### PLAYERS

`find_player(3)` finds players in radius 3 around robot and returns list, if none returns nil
`attack(target)` attempts to attack target player if nearby
`grab(target)` attempt to grab target player if nearby and returns true if succesful
`player.getpos(name)` return position of player, player.connected() returns list of players

### ROBOT

`say(\"hello\")` will speak
`self.listen(0/1)` (de)attaches chat listener to robot
`speaker, msg = self.listen_msg()` retrieves last chat message if robot listens
`self.send_mail(target,mail)` sends mail to target robot
`sender,mail = self.read_mail()` reads mail, if any
`self.pos()` returns table {x=pos.x,y=pos.y,z=pos.z}
`self.name()` returns robot name
`self.set_properties({textures=.., visual=..,visual_size=.., , )` sets visual appearance
`set_animation(anim_start,anim_end,anim_speed,anim_stand_start)` set mesh animation
`self.spam(0/1)` (dis)enable message repeat to all
`self.remove()` stops program and removes robot object
`self.reset()` resets robot position
`self.spawnpos()` returns position of spawner block
`self.viewdir()` returns vector of view for robot
`self.fire(speed, pitch,gravity)` fires a projectile from robot
`self.fire_pos()` returns last hit position
`self.label(text)` changes robot label
`self.display_text(text,linesize,size)` displays text instead of robot face, if no size return text
`self.sound(sample,volume)` plays sound named 'sample' at robot location
rom is aditional table that can store persistent data, like `rom.x=1`
.
### KEYBOARD
place spawner at coordinates (20i,40j+1,20k) to monitor events

`keyboard.get()` returns table {x=..,y=..,z=..,puncher = .. , type = .. } for keyboard event
`keyboard.set(pos,type)` set key at pos of type 0=air, 1..6, limited to range 10 around
`keyboard.read(pos)` return node name at pos

### TECHNIC FUNCTIONALITY
namespace 'machine'. most functions return true or nil, error

`machine.energy()` displays available energy
`machine.generate_power(fuel, amount)` = energy, attempt to generate power from fuel material
	if amount>0 try generate amount of power using builtin generator - this requires 40 gold/mese/diamonblock upgrades for each 1 amount
`machine.smelt(input,amount)` = progress/true. works as a furnace
	if amount>0 try to use power to smelt - requires 10 upgrades for each 1 amount, energy cost is 1/40*(1+amount)
`machine.grind(input)`  grinds input material, requires upgrades for harder material
`machine.compress(input)` requires upgrades - energy intensive process
`machine.transfer_power(amount,target_robot_name)`

### CRYPTOGRAPHY
namespace 'crypto'

`crypto.encrypt(input,password)` returns encrypted text, password is any string
`crypto.decrypt(input,password)` attempts to decrypt encrypted text
`crypto.scramble(input,randomseed,sgn)`  (de)permutes text randomly according to sgn = -1,1
`crypto.basic_hash(input,n)` returns simple mod hash from string input within range 0...n-1
