<!-- --------------------------------------------
-- title : Introduction à la robotique
-- author : 
-- description : Guide de l'usage est des comportements des Robots.
-- source : parts from https://wiki.minetest.net/Mods/basic_robot
------------------------------------------------- -->

# À propos des robots
Ce document présente les comportements des robots ainsi leurs limitations et usages. Pour des instructions détaillées à propos de la programations des robots voir [le manuel technique](#robots_api.fr.md)

## Introduction

Les robots peuvent se déplacer, détécter des blocs, creuser, construire et plus encore.

Leurs utilisateurs peuvent écrire des programmes pour le robot dans le langage 'Lua'.
Il existe aussi une télécommande.

Le système consiste en un 'spawner' ('cpu-box') que les joueurs posent sur le sol.
Ce spawner propose un formulaire pour contrôler, programmer et faire fonctionner le robot ('worker').
Seule cette partie ('worker') du robot se déplace et dispose d'un rayon d'action limité (environ 23 blocs du joueur et du spawner).

Un clavier du mod basic_machines peut être programmé pour démarrer le robot via un bouton presser par d'autres joueurs et pour recevoir des entrées clavier.
C'est utile pour les robot qui executent des actions pour d'autres joueurs (ex. magasin, messagerie, robot parlant, mini-jeu, etc. ) 

Sommairement, on peu dire que les robots sont constitué des deux parties mentionnée plus haut :vous pouvez entrer du code dans le spawner ('cpu-box') et l'executer avec le bouton start. Le 'worker' apparaîtra au dessus du spawner et executera le code depuis cette position.

Il est parfois plus simple d'utiliser le terme 'robot' pour désigner la partie 'worker' dur robot.
En disant par exemple : Le spwaner ressemble à une boite-cpu tandis que le robot ressemble à un simple bloc blanc avec une tête sur sa face avant et une flêche sur le dessus.

## Comment obtenir un robot ?

Vous pouvez crafter le robot avec sur cristaux de mese au dessus d'une pierre, un lingot d'acier, et une autre pierre
Vous pouvrez crafter une télécommande avec un crisal de mese sous un bâton.

## Le formulaire de contrôle

Un clic-droit sur le spawner ouvre un formulaire composé de boutons et d'une zone de texte destinée à recevoir le programme du robot.

* Le boutton "Save" enregistre le programme. Vous devez sauvegardez avant de lancer le programme. 

  Note: La sauvegarde n'est pas permanente : une verification syntaxique est réalisées mais aucun fichier n'est enregisté et rammasser le spawner effecera le contenu de cette zone de texte. 
  Copier et collez ce texte ailleurs pour ne pas le perdre.
  
* Le boutton "Start" crée un robot-worker (au dessus du spawner) qui execute le programme. 

* Le boutton "Stop" interrompt le programme et supprime le robot-worker. 

* Le champ 'id permet de controller plusieurs robots depuis un spawner. 
  Les joueurs qui ne disposent pas du privilège 'robot'  ne peuvent utiliser que 2 robots à la fois. Ce qui signifie qu'ils ne peuvent utiliser que les ids 1 and 2.

* Le boutton "Inventory" ouvre l'inventaire du robot. 
  L'inventaire contient 8*4 emplacements, la même taille qu'un coffre. Les robots peuvent stocker de choses dans cet inventaire (ex. les matériaux récoltés en creusant), et peuvent aussi prendre des choses dans cet inventaire (ex. pour produire de l'énérgie)

* Le boutton "Library" ouvre une "bibliothèque". Le robot peut lire et écrire les livres qui s'y trouvent. 
  Cela peut être utilisé pour stocker et accèder facilement à du code depuis le programme. 

* Le boutton "Help" affiche une aide pour les fonctions du robot.

Utilisez la touche Echap pour quitter le formulaire. 


### Notes

Chaque spawner ne peut activer qu'un robot à la fois.
Pour rammasser le spawner, son inventaire et sa bibliothèque doivent être vides. Mais le robot est capable de casser le sapwner, son contenu sera alors perdu.

Lorsque que le robot est en fonction, un clic-droit dessus oouvrira egalement le formulaire (sans le bouton 'start') vous permettant ainsi de le stoper ou d'accèder à son inventaire.

Le code du programme est exectuté en boucle répétée environ toutes les secondes

L'edition du programme d'un robot en fonctionnement ne prendra effet qu'après l'avoir stoppé et relancé. 

 
## Capacitées 

#### * Energie

Le robot à besoin d'énérgie pour effectuer certaines actions telles que creuser, fondre, écraser ou compresser.
Si le robot dispose d'un combustible (ex: un morceau de charbon) dans son inventaire, il peut être programmé pour produire de l'énérgie : `machine.generate_power("default:coal_lump")` 
Le niveau actuel d'énérgie peut être consulté avec la fonction `machine.energy()`.
<!-- Note : Write about upgarde -->

#### * Limite d'actions

Comme mentionné plus haut, le programme du robot s'execute en boucle. Cela signifie qu'à moins de recevoir l'instruction de s'arrèter, le robot executera le code encore et encore... aussi longtemps qu'il le peut.

Pour cette raison, certaines opérations disposent d'un nombre d'execution limité par boucle. Ces actions incluents celles mentionnées plus haut, ainsi que quelques autres (ex. generer de l'energie).
<!-- Note : A complete list would be good !-->

Lorsque cette limite est atteinte, le code refusera de s'executer et le robot reverra le message d'erreur `robot out of available operations in one step.`.
Cette limite est parfois appelée 'maxdig' ou 'maxoperations' (le nom de la variable correspondante).
<!-- Note : Mabe store maxoperations in self in order to consult it in-gameb -->
<!-- Note : Setting the limit to 3 might be a good thing too -->

La limite par defaut est 2. Ce qui signifie que lorsque le robot démarre, il doit y avoir moins de 2 (=1) opération dans une boucle
Par exemple, je ne peux pas executer ce code :


> machine.generate_power("default:jungletree") 	-- operation limitée 
> dig.forward()									-- operation limitée
> move.forward()
> dig.up()										-- operation limitée

Selon ce code, le robot devrait générer de l'énergie à partir d'arbre de jungle, creuser vers l'avant, se déplacer vers l'avant, et creuser vers le haut et ... repeter.
Mais cela ne fonctionnera pas car 3 actions limitées seraient éxécutées en une boucle.

L'astuce serait d'incrémenter un nombre afin de diviser l'action en étapes, comme ceci :


> if not i then i = 1 end 	--initialise i
> if i == 1 then 		machine.generate_power("default:jungletree") 	
> elseif i == 2 then 	dig.forward()									
> 						move.forward()
> elseif i == 3 then	dig.up()										
>						i = 0
> end											
> i = i + 1											

Dans cet exemple, 'i' prend d'abord la valeur 1 et sa valeurs sera augmenté à chaque boucle (à cause de la dernière ligne)

La première fois que le code est executé, 'i' vaut 1, de l'énérgie sera générée.
    
La seconde fois que le code est executé, 'i' vaudra 2, le robot creusera vers l'avant et se déplacera vers l'avant, 

La troisième fois, 'i' vaudra 3, le robot will creusera vers le haut et réinitialisera la valeur de i à 0 ( de façon à ce qu'après la dernière ligne i sera égal a 1. )

Donc la quatrième fois que le code sera executé sera identique à la première fois, la cinquième sera comme la seconde, la sixième sera comme la troisième, la septième comme la première...  et ainsi de suite.


#### * Gravité

Bien qu'il ne semble pas être sujet à la gravité, le robot ne peut pas léviter au dessus de plus d'un bloc d'air.

Si vous lui demandez d'avancer en direction d'une falaise, il ignorera simplement l'operation qui le placerai dans une situation de levitation au dessus de deux blocs d'air ou plus...

... mais il continuera d'executer le programme en ignorant totalement les conséquences - parfois catastrophiques - de cette étape sautée !

Si malgré cette prudence, le robot se retouve au dessus du vide, il s'arrêtra simplement et disparaîtra

#### * Le bruit de l'arbre qui tombe...

Contrairement à la question étérnelle 'Est ce qu'un arbre qui tombe fait du bruit s'il n'ya personne pour l'entendre?', nous pouvons répondre à celle-ci : 'Est ce q'un robot-worker travaille s'il n'y personne pour le voir ?'.

Et la réponse est NON.

Dans minetest, les zone qui ne sont pas peuplées (de joueurs) ne sont pas chargées. 
Donc, si vous partez loins de votre robot (ou de son spawner), en le laissant travailler, il ne tardera pas à s'arrêter.

Mais il y a pire ! Lorsque vous reviendrez, la zone où se trouve le robot sera à nouveau chargée et celui-ci reprendra son programme AU DEBUT, sans considération pour la ligne à laquelle il s'était arrèté...

Ce qui, encore une fois, peut résuter en un désordre catasrophique à cause de quelques lignes sautées.

Donc, ... voilà


## Conclusion 

L'usage des robots s'apprends par les essais et erreurs.

Le [le manuel technique](#robots_api.fr.md) devrait aussi être utile.

Vous devriez aussi consulter le wiki :  
and la page du forum : https://forum.minetest.net/feed.php?f=3&amp;t=18345
and essayer le serveur Basic_Robot.
