<!-- --------------------------------------------
-- title : About Robots - The short version
-- author : 
-- description : Short Guide to Robots Usage
-- source : parts from https://wiki.minetest.net/Mods/basic_robot
------------------------------------------------- -->

# Things to know about Robots
A few things to know about robots, summarized in a short lists

* The robot can do a lot of things, They execute programs written in Lua.
* There is also a remote control.

* You can write code in the spawner ('cpu-box') and run it with the 'start' button. The 'worker' will appear above the spawns block and execute the code from there.

* The spwaner looks like a ... cpu-box while the robot looks like a simple box, with a face on its frontside, and an arrow on top. 

* Standard users are allowed to control 2 robots. Each spawner can only run one bot at a time.


*  Robot can store things to its inventory (e.g. materials gathered while digging), they can also take thing from it (e.g. to produce energy)

* The robot can read and write books contained in their 'library'.  This can be used for program code, input and output. 


* To pick up the spawner, its inventory and library must be empty (like a chest).

* The code of a program is executed repeatedly, about once every second.

* The robot require energy to perform some actions like digging, smelting, grinding or compressing.

* Some operations can only be performed a limited number of time per run otherwise the robot will return this error message : `robot out of available operations in one step.`
  (The trick would be increment a number to divide the steps, like this )

* The robot cannot hover over more than one block of air (and will skip the instruction to do so).
	
* If you wander off leaving your robot to its work, it will stop and restart its program AT THE BEGINNING when you come back.
