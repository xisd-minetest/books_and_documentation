<!-- --------------------------------------------
-- title : Mémo à propos des robots
-- author : 
-- description : Short Guide to Robots Usage
-- source : parts from https://wiki.minetest.net/Mods/basic_robot
------------------------------------------------- -->

# Ce qu'il faut savoir à propos des robots

Quelques informations utiles à propos des robots

* Les robots peuvent faire de nombreuses choses, ils executent des programmes écrits en 'Lua'.
* Il y a aussi une télécommande.

* Vous pouvez écrire du code dans le spawner ('cpu-box') et l'executer avec le bouton 'start'. Le 'worker' apparaitra alors au dessus du spawner et executera le programme.

* Le spwaner ressemble à une boite-cpu tandis que le robot est un bloc blanc avec une tête dessinée sur sa face avant et une flèche sur le dessus. 

* Les utilisateurs standard peuvent controler 2 robots. Chaque spawner ne contrôle qu'un robot à la fois.


* Le robot peut stocker des choses dans son inventaire (ex. matériaux creusés), il peut aussi prendre des choses dans cet inventaire (ex. pour produire de l'énérgie)

* Le robot peut lire et écrire les livres présent dans sa bibliothèque. Ce qui est utile pour accèder facilement à des sous-programmes. 

* Pour rammasser le spawner, son inventaire et sa bibliothèque doivent être vides. Mais le robot est capable de casser le sapwner, son contenu sera alors perdu.

* Le code d'un programme est executé en boucle, une fois par seconde environ.

* Le robot à besoin d'énérgie pour effectuer certaines actions telles que creuser, fondre, écraser ou compresser.

* Si la boucle contient un nombre trop important d'opérations limitées, le code refusera de s'executer et le robot reverra le message d'erreur `robot out of available operations in one step.`.
  (L'astuce serait d'incrémenter un nombre afin de diviser l'action en étapes. )

* Le robot ne peut pas léviter au dessus de plus d'un bloc d'air (et ignorera les intructions en ce sens).
	
* Si vous partez en laissant votre robot travailler seul, il s'arrêterra et reprendra son programme au DEBUT à votre retour.
