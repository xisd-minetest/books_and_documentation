<!-- --------------------------------------------
-- title : Robots, what are they ?
-- author : 
-- description : Guide to understand Robots usages and behaviors
-- source : parts from https://wiki.minetest.net/Mods/basic_robot
------------------------------------------------- -->

# About Robots
This document present robots behaviors, limitations and usages, for detailled functions and syntax, see 'Robot API'

## Introduction

The robot can move around, sense the blocks, dig, build, and much more.

Users can write programs for the bot in Lua. There is also a remote control.

The system uses a spawner ('cpu-box') that the player has to place on the ground.
This spawner presents a form for controlling, programming and running the robot ("worker").
Only this worker moves around, and it has a limited range (about 23 nodes from player and spawner).

A keypad from the mod basic_machines can be set up to start a robot with a button-push by other players, and for keyboard-entry.
This is useful for robots that perform a task for other players, e.g. as a shop, mailbox, chatbot, game, etc. 

Mainly, robots are constituted of the two part mentionned above : you can write code in the spawner ('cpu-box') and run it with the sart button. The 'worker' will appear above the spawns block and execute the code from there.

It is someting easier to call 'robot' the worker part of the robot, saying think like :
The spwaner looks like a ... cpu-box while the robot looks like a simple box, with a face on its frontside, and an arrow on top.

## How to get it ?

You can craft a robot using 6 mese crystals atop a stone, a steel ingot, and another stone.
You can craft a remote control using mese crystal under a stick

## Controlling Form

Rightclicking the spawner opens a form with buttons and a textbox for writing a program.

* Button "Save" saves the program. This must be done before starting a program. 

  Note: Saving is not permanent - there is no file to save to, just a quick syntax-check. 
  Also, picking up the spawner clears the textarea with the code. 
 So, use cut and paste to save your code to a texteditor, such as notepad. 

* Button "Start" creates a robot-worker (on top of the spawner) that starts executing the program. 

* Button "Stop" stops the program and removes the robot-worker. 

* Entryfield id for controlling several robots. 
  Players without the priv 'robot' are only allowed to run 2 robots at the same time. 
  This means they can only use the ids 1 and 2.

* Button "Inventory" opens the inventory of the bot. 
  The inventory has 8*4 slots, same size as a chest. Robot can store thing to its inventory (e.g. materials gathered while digging), they can also take thing from it (e.g. to produce energy)

* Button "Library" opens a "bookshelf". The robot can read and write books here. 
  This can be used for program code, input and output. 

* Button "Help" shows some helptext.

Press ESC to exit the form. 


### Notes

Each spawner can only run one bot at a time.
To pick up the spawner, its inventory and library must be empty (like a chest).

While the robots is running, rightclicking it will also open the form 
(without the Start-button), so you can stop it, and access its inventory. 

The code of a program is executed repeatedly, about once every second.

After editing the program of a running robot won't affect it until it is stopped and restarted. 

 
## Capabilities 

#### * Energy

The robot require energy to perform some actions like digging, smelting, grinding or compressing.
If the robot as the required fuel in its inventory (e.g. a coal lump ), it can be programmed to produce energy using `machine.generate_power("default:coal_lump")` 
Energy level can be consulted using `machine.energy()`.
<!-- Note : Write about upgarde -->

#### * Maxdig

As said above, the robot program runs in a loop. This means that unless specificly asking it to stop, the robot will execute the code again and again and again... as long as it can. 
Because of this, some operations can only be performed a limited number of time per run. These actions includes the ones mentionned above along with some other (including generating energy).
<!-- Note : A complete list would be good !-->

If this limit is reached, the code won't execute and the robot will return this error message : `robot out of available operations in one step.`
The limit is sometime called 'maxdig' or 'maxoperations' (as the name of the variable defining it).
<!-- Note : Mabe store maxoperations in self in order to consult it in-gameb -->
<!-- Note : Setting the limit to 3 might be a good thing too -->
The default for this limit is 2. This means that when the robot is starting, there must be less than 2 operations in one loop.
For exemple, I cannot run this programm :


> machine.generate_power("default:jungletree") 	-- limited operation 
> dig.forward()									-- limited operation
> move.forward()
> dig.up()										-- limited operation

According to this code, the robot should, generate some power from some jungle tree, dig forward, move foward, dig upwards and ... repeat itself. But it won't do any of that because that 3 limited operations would be performed in one execution

The trick would be increment a number to divide the steps, like this :

> if not i then i = 1 end 	--initialize i
> if i == 1 then 		machine.generate_power("default:jungletree") 	
> elseif i == 2 then 	dig.forward()									
> 						move.forward()
> elseif i == 3 then	dig.up()										
>						i = 0
> end											
> i = i + 1											

In this exemple 'i' is set to 1 initially, and its value will be upped each time code is executed (because of the last line).
The first time the code is executed, 'i' is set to one, power will be generated    
The second time the code is executed, 'i' will be egal 2, the robot will dig forward and move forward, 
The third time the code is executed, 'i' will be egal 3, the robot will dig up and reset the value of i to 0 so that after last line, it will be egal 1.
So the fourth time is like the first, the fifth is like the second, the sixth is like the third, and so on...


#### * Gravity

Altough it doesn't see to be subject to gravity, the robot cannot hover over more than one block of air.
If asked to move toward a cliff, the robot will simply ignore the operation that would put him in a situation with too much void under...
... but it will continue executing its program regardless of the - sometme catastophic - consequences of this skipped step !

<check> If despite this beahior, the robot ends up above more than one block of air, it will simply stop and disapear.

#### * The sound of a falling tree

Quite unlike the eternal question 'Does a falling tree make sound if there is no one to hear it ?', we can answer this one : 'Does a robot worker works when there is no one around to see it ?'. And the answer is NO.

In minetest, areas that are not populated (with players), are unloaded. 
So if you wander off leaving your robot to its work, it will soon stop working. But there is worse !
When you come back, you robot will be loaded again and will restart its program AT THE BEGINNING, regardless to were it stopped. Resulting (again) in a giant mess due to a few steps skipped. So, ... Be warned


## Conclusion 

Using robot is a matter of trial an error, the 'Robot Api' document should also be useful.
You might also want to consult minetest wiki :  
and the forum page : https://forum.minetest.net/feed.php?f=3&amp;t=18345
and try the Basic_Robot Server
