-- This file add some special book to written books sélection
-- 
if not minetest.get_modpath("written_books") then return end

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local worldpath = minetest.get_worldpath()

-- Load support for intllib.
-- local S, NS = dofile(modpath.."/intllib.lua")
--	S = function(s) return s end

-- TODO -- about_portals.md","The art of Elemental Teleportation","---")
-- TODO -- about_mesecons.md","Mesecons Circuitery","---")
-- TODO -- about_mobs_monster.md","Monsters of the world","---")
-- TODO -- about_mobs_animals.md","Pets and beasts","---")
-- TODO -- about_tools.md","Mining : Tools","---")
-- TODO -- about_farming.md","A vegetables story","---")
-- TODO -- about_magic.md","Wizzardery and Withccrafting","---")

-- Path for written book from file content
local path = {}
path.docs = modpath.."/docs"
path.books = path.docs.."/books"
path.cookbooks = path.docs.."/cookbooks"
path.robots = path.cookbooks.."/cookbook_robots"
path.circuits = path.cookbooks.."/cookbook_circuits"



local books ={
	{path = path.robots, basename = "robots_api", ext="md"}, -- Book version
	{path = path.robots, basename = "robots_api", ext="md",alernate="scroll"}, -- Scroll Version
	{path = path.robots, basename = "about_robots", ext="md"}, -- Book version
	{path = path.robots, basename = "about_robots", ext="md",alernate="scroll"}, -- Scroll Version
	{path = path.robots, basename = "about_robots_short", ext="md"},
	{path = path.robots, basename = "about_robots_short", ext="md",alernate="scroll"},
}

for _,def in ipairs(books) do
	
	if not def.path then def.path = path.books end

	written_books.register_book_from_file(def)
	
	-- Get file path
	--local fp = path.books.."/"..b[1]..".md"
	--written_books.register_book_from_file(fp)
end
