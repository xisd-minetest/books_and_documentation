#!/bin/sh
# Generate a html page from all the markdown documents present in rep 'docs'
# Require 'markdown' command
#

#-- Markdown docs path
docpath="../docs/handbook"
if [ ! -d "$docpath" ];then exit ;fi
#-- output file -- must be in same rep as md files (for relative links to work)
output="${docpath}/doc.html"
# if [ -f "$output" ];then echo "The output file '${output}' already exist"; exit; fi


#-- Temporary files
html_menu_tmp="~html_doc_menu.tmp"
html_content_tmp="~html_doc_content.tmp"
if [ -f "$html_menu_tmp" ];then 
	echo "The temporary destination file '$html_menu_tmp' already exist. Aborting"
	exit
fi
if [ -f "$html_content_tmp" ];then 
	echo "The temporary destination file '$html_menu_tmp' already exist. Aborting"
	exit
fi

#-- Initialize files
echo "<h1>Documentation</h1>" > "$html_menu_tmp"
echo "" > "$html_content_tmp"
echo "<a name='top'></a><ul>" >> "$html_menu_tmp"


#-- Loop through file
listfilename="liste"
list="${docpath}/${listfilename}"
ls $docpath |grep -e "^.*\.md$" > $list
files=$(cat $list |grep -v $listfilename)
for mddoc in $files ; do
	#-- Full path
	doc="${docpath}/${mddoc}" 

	#-- Get infos
	pat_title="\-\- title :"
	title=$(grep "$pat_title" $doc |sed "s#$pat_title##")
	pat_description="\-\- description :"
	description=$(grep "$pat_description" $doc |sed "s#$pat_description##")

	#-- Menu entry
	echo "	<li><a href=\"#${mddoc}\">${title}</a>" >> "$html_menu_tmp"
	
	#	echo 	"<br> - ${description}</li>" >> "$html_menu_tmp"
	echo 	"</li>" >> "$html_menu_tmp"

	#-- Anchor
	echo "<hr>" >> "$html_content_tmp"
	echo "<a name=\"${mddoc}\"></a><a href=\"#top\">-- Retour au menu --</a><br>" >> "$html_content_tmp"

	#-- Content
	markdown "$doc" >> "$html_content_tmp"
	echo "" >> "$html_content_tmp"
done
echo "</ul>" >> "$html_menu_tmp"

#-- Join files
echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
<title>Minetest Azoth</title>
<meta name="Description" content="Documentation pour minetest Azoth">
<link media="all" href="styles.css" type="text/css" rel="stylesheet">
</head>' > "$output"
cat "$html_menu_tmp" >> "$output"
cat "$html_content_tmp" >> "$output"

#-- Delete temporary files
rm -f "$html_menu_tmp"
rm -f "$html_content_tmp"
