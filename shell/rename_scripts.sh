#!/bin/sh
# Rename scripts to be readable by lua as <prefix>_<number>.lua
# And add title and author as comment at the beginning of the file
#
# Arguments : 
### 1 : path of directory  containing scripts
### 2 : prefix
### 3 : number to start numerotation
### 4 : author of the scripts
### 5 : source of the scripts
### 6 : description of the scripts



dirpath="$1"
prefix="$2"
n="$3"
author="$4"
source="$5"
description="$6"
help="Usage : 
rename_scripts <folder_containing_scripts> <prefix> <number_to_start_numerotation> <author_of_scripts> <source_of_scripts> <description_of_scripts>"

#-- Return if not a valid directory
if [ ! -d "$dirpath" ]; then
	echo "Wrong argument 1 (directory path) :'$dirpath' "
	echo "$help"
	exit
fi
#-- Return if prefix is empty
if [ -z "$prefix" ]; then
	echo "Argument 2 (prefix) is empty and must be definined"
	echo "$help"
	exit
fi
#-- Set n to 1 if empty
if [ -z "$n" ]; then n=1 ;fi 

for scp in $(ls $dirpath |grep -e "\.lua$"); do
	title=$(basename --suffix=.lua $scp) 
	in="${dirpath}/${scp}"
	out="${dirpath}/${prefix}_${n}.lua"

	echo "-- title : $title" > "$out"
	echo "-- author : $author" >> "$out"
	echo "-- description : $description" >> "$out"
	echo "-- source : $source" >> "$out"
	echo "--" >> "$out"
	echo "" >> "$out"
	cat "$in" >> "$out"

	rm "$in"
	n=$((n+1))
done
